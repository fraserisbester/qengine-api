from json import loads
from base64 import urlsafe_b64decode
from datetime import datetime
import logging

class JWT:

    def __init__(self, encoded_jwt):
        self.encoded_jwt = encoded_jwt
        self.expired_on_call = None

        self._decode_jwt()

        self.valid = self._validate()

    def _validate(self):

        conds = []

        # Check expiration
        if "payload" in self.decoded_jwt.keys():
            if datetime.now().timestamp() > self.decoded_jwt["payload"]["user"]["exp"]:
                return True
        else:
            return False


    def _decode_jwt(self):

        encoded_jwt_header, encoded_jwt_payload, encoded_jwt_signature = self.encoded_jwt.replace("-", "+").replace("_", "/").split(".")

        jwt = {}

        # Parse Header
        # try:
        bytes_header = urlsafe_b64decode(encoded_jwt_header.strip() + "==")
        decoded_header = bytes_header.decode("utf-8", "ignore")
        dict_header = loads(decoded_header)
        # except UnicodeDecodeError:
        #     logging.warning({"warning": UnicodeDecodeError})
        # except Exception:
        #     logging.warning({"warning": Exception})
        # else:
        jwt["header"] = dict_header

        # Parse Payload
        # TODO: Fail on fail
        # try:
        bytes_payload = urlsafe_b64decode(encoded_jwt_payload.strip() + "==")
        decoded_payload = bytes_payload.decode("utf-8", "ignore")
        dict_payload = loads(decoded_payload)
        # except UnicodeDecodeError:
        #     # raise UnicodeDecodeError
        #     logging.error({"Error": UnicodeDecodeError})
        # except Exception:
        #     logging.error({"Error": Exception})
        # else:
        jwt["payload"] = dict_payload

        # Parse Signature
        # TODO: Validate
        try:
            bytes_signature = urlsafe_b64decode(encoded_jwt_signature.strip() + "==")
            decoded_signature = bytes_signature.decode("utf-8", "ignore")
            dict_signature = loads(decoded_signature)
        except UnicodeDecodeError:
            logging.warning({"warning": UnicodeDecodeError})
        except Exception:
            logging.warning({"warning": Exception})
        else:
            jwt["signature"] = dict_signature

        self.clean_decode = True if "header" in jwt.keys() and "payload" in jwt.keys() and "signature" in jwt.keys() else False
        self.decoded_jwt = jwt