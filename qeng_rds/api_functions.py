
# Layer imports
from coredb.rds.bump.models import(
    Lot,
    DeviceType,
    ProductSKU,
    DeviceInventory,
    User,
    Pharmacy,
    Association
)

# Util imports
from json import dumps, loads
from datetime import datetime, timedelta
from base64 import urlsafe_b64decode
from sqlalchemy import or_, func

class APIResponse:

    def __init__(self):
        self.body = False

        #: Integer Code of responded HTTP Status, e.g. 404 or 200.
        self.status_code = None

        #: Dictionary of Response Headers.
        self.headers = {}

        # Dictionary of semistructured query metadata
        self.metadata = {}

        #: Encoding to decode with when accessing r.text.
        self.encoding = None

        #: Textual reason of responded HTTP Status, e.g. "Not Found" or "OK".
        self.reason = None

        #: The amount of time elapsed between sending the request
        # self.elapsed = datetime.timedelta(0)

    def __repr__(self, target="proxy"):

        response_dict = {
            "headers": self.headers,
            "metadata": self.metadata,
            "body": self.body
        }
        return response_dict

    def __dict__(self, target="proxy"):

        if target == "proxy":
            return {
                "isBase64Encoded": True if self.encoding == "base64" else False,
                "statusCode": self.status_code,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    **self.headers},
                "body": dumps(self.body, default=str)
            }
        else:
            raise KeyError(f"Unknown target {target}")


def query_string_parser(event, query_key) -> list:

    multi_query_params = event["multiValueQueryStringParameters"]

    # Handle either split case, or Angular case
    if len(multi_query_params[query_key]) > 1:
        filter_buffer = multi_query_params[query_key]
    else:
        filter_buffer = multi_query_params[query_key][0].split(",")

    # Handle Unique case of inverted (MAC) serial numbers
    if query_key == "serial_number":
        filter_buffer = [sn if sn[2::3] != ":::::::" else sn[::-1] for sn in filter_buffer]

    return filter_buffer

def proxify_response(code, body, **headers):
    """Lambda Proxy takes a different format than Lambda, this builds that.

    :param code: The HTTP response code to pass forward
    :type code: int
    :param body: The Body (content) of the response
    :type body: dict
    :return: Lamnda Proxy response
    :rtype: dict
    """

    return {
        "isBase64Encoded": False,
        "statusCode": code,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            **headers},
        "body": dumps(body, default=str)
    }

def easy_pagination(event, page=0, page_size=1000):

    pagination_errors = {}
    if event["queryStringParameters"]:

        query_params = event["queryStringParameters"]

        if "page" in query_params.keys():
            if not query_params["page"].isdigit():
                pagination_errors['page_errors'] = [f"'{query_params['page']}' malformed for target 'page' - Expected positive integer"]

            if "page_errors" not in pagination_errors.keys():
                page = int(query_params["page"])

        if "page_size" in query_params.keys():
            page_size_errors = []
            if not query_params["page_size"].isdigit():
                pagination_errors['page_size_errors'] = [f"'{query_params['page_size']}' malformed for target 'page_size'"]
            if int(query_params["page_size"]) <= 0:
                pagination_errors['page_size_errors'] += [f"'{query_params['page_size']}' must be > 0 for target 'page_size'"]
            if "page_size_errors" not in pagination_errors.keys() :
                page_size = int(query_params['page_size'])

    return (page, page_size, pagination_errors)

def query_stringer(**kwargs):
    # Build a querystring from arbitrary k/v pairs

    q_str = "?"
    for index, (key, value) in enumerate(kwargs.iteritems()):
        q_str += str(key) + "=" + str(value) + "&"

    return q_str[:-1]

class Response:
    """Response object for dynamic representation of response object
    """

    def __init__(self, platform="APIGProxy", isBase64Encoded=False):
        self.platform = platform
        self.isBase64Encoded = isBase64Encoded
        self.metadata = {}
        self.data = []
        self.status = None

    def __str__(self):

        body = {
            "metadata": self.metadata,
            "data": self.data
        }

        if self.isBase64Encoded:
            body = str(body).encode("base64", "strict")

        if self.platform == "APIGProxy":
            struct = {
                "isBase64Encoded": self.isBase64Encoded,
                "statusCode": self.status,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                "body": dumps(body, default=str)
            }
        else:
            raise ValueError(f"Unknown platform: {self.platform}")

        return struct


class ResourceMap:

    def __init__(self, view):
        self.view = view
        self.data = self._define_resources()

    def objs(self, returnable_only=True):
        return [resource["obj"] for ref, resource in self.data.items() if resource["return"] == returnable_only]

    def loc(self, target_resource):
        return list(self.data.keys()).index(target_resource)

    def names(self):
        return [ref for ref, resource in self.data.items()]

    def filters(self, event, target="DB"):

        # Check filters
        if not event["multiValueQueryStringParameters"]:
            return []

        # Filter target
        target = target.upper()

        # filter_list
        filter_list = []

        if target == "DB":

            # Iterate over DB filterable items
            for reference, column in self.data.items():

                if column["filter"] == "DB":

                    # Ensure the filter is approved for this case
                    if reference in event["multiValueQueryStringParameters"].keys():

                        # Handle as date case
                        # TODO: extract with reference column Types
                        # IE: if column.type == sqlalchemy.DateTime
                        if reference == "date_added":

                            filter_buffer = query_string_parser(event, reference)

                            if len(filter_buffer) == 1:
                                # Pull from date
                                filter_buffer[0] = datetime.strptime(filter_buffer[0], "%Y-%m-%d")
                                # Create to date
                                filter_buffer.append(filter_buffer[0] + timedelta(days=1))
                            elif len(filter_buffer) == 2:
                                filter_buffer[0] = datetime.strptime(filter_buffer[0], "%Y-%m-%d")
                                filter_buffer[1] = datetime.strptime(filter_buffer[1], "%Y-%m-%d")
                            filter_list.append(column["obj"].between(filter_buffer[0], filter_buffer[1]))

                        else:
                            filter_buffer = query_string_parser(event, reference)
                            discreet_filters = []
                            wildcard_filters = []

                            for single_filter in filter_buffer:
                                if "_" in single_filter or "%" in single_filter:
                                    wildcard_filters.append(column["obj"].ilike(single_filter))
                                else:
                                    discreet_filters.append(single_filter)

                            filter_list.append(or_(
                                        *wildcard_filters,
                                        column["obj"].in_(discreet_filters)
                                    ))
                else:
                    # Exclude this column from the filtering
                    pass

        # Handle filter to be applied in memory
        elif target == "MEM":
            return filter_list
        else:
            raise ValueError(f"Unknown filter target: {target}")

        # Return constructed list of filters
        return filter_list

    def orderables(self, event, target="DB"):

        # Check orderables
        if not event["multiValueQueryStringParameters"]:
            return []
        if "order_by" not in event["multiValueQueryStringParameters"]:
            return []

        target = target.upper()

        order_buffer = query_string_parser(event, "order_by")
        order_list = []

        if target == "DB":

            # Get keys for DB
            db_keys = []
            for reference, resource in self.data.items():
                if resource["order"] == "DB":
                    db_keys.append(reference)

            for order_item in order_buffer:
                ref, direction = order_item.split(".")

                if ref in db_keys:
                    if direction == "asc":
                        order_list.append(self.data[ref]["obj"].asc())
                    elif direction == "desc":
                        order_list.append(self.data[ref]["obj"].desc())
                    else:
                        ValueError(f"Unknown sort direction: {direction}")
                else:
                    pass

        elif target == "MEM":
            pass
        else:
            raise ValueError(f"Unknown sort target: {target}")

        # return constructed list of orders
        return order_list

    def _input_validation(self):
        # Handle resource input validation and transformation where required
        pass

    def _define_resources(self) -> dict:
        """Definds SQLAlchemy resources and what they can be used for
        filter and orders should be categotrical [DB, MEM]

        :return: Map of every touchable resource
        :rtype: dict
        """

        if self.view == "inventory":
            field_resource_map = {
                "device_type": {
                    "obj": ProductSKU.product_type.label("device_type"),
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "date_added": {
                    "obj": DeviceInventory.created_at.label("date_added"),
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "serial_number": {
                    "obj": DeviceInventory.serial_number,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "lot_name": {
                    "obj": Lot.lot_name,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "assigned_user": {
                    "obj": User.subject_id.label("assigned_user"),
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "sku": {
                    "obj": ProductSKU.sku.label("tla"),
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "device_details": {
                    "obj": DeviceInventory.device_details,
                    "return": False,
                    "filter": "DB",
                    "order": "DB"
                },
                "status": {
                    "obj": DeviceInventory.status,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "imei": {
                    "obj": func.json_unquote(DeviceInventory.device_details["imei"]).label("imei"),
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "iccid": {
                    "obj": func.json_unquote(DeviceInventory.device_details["iccid"]).label("iccid"),
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "mac_address": {
                    "obj": func.json_unquote(DeviceInventory.device_details["mac_address"]).label("mac_address"),
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                }
            }

        elif self.view == "users":
            field_resource_map = {
                "assigned_user": {
                    "obj": User.subject_id,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "user_date_added": {
                    "obj": User.date_added,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "serial_number": {
                    "obj": DeviceInventory.serial_number,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "assigned_date": {
                    "obj": Association.start_date,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                },
                "unassigned_date": {
                    "obj": Association.end_date,
                    "return": True,
                    "filter": "DB",
                    "order": "DB"
                }
            }

        return field_resource_map