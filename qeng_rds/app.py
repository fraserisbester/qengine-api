
# TODO: Auth
# TODO: Query Logging
#           - QueryCalls: User | Role | DateTime | Query/QueryID | Response code | Execution Time
#           - QueryLookup: QueryID | Query string (V2)
# TODO: Effeciency:
#           - Subquery filter before coplete join
#           - Eager joined load?
#           - Shadow load unused cols / define only needed entities
#           - header mask + raw data to drop payload size
#           - Partial Resource responsing OR better, Marshmallow

# Core imports
from datetime import datetime, timedelta
from sys import getsizeof
from itertools import groupby
from operator import attrgetter
from  json import dumps

import io, csv, zlib, sys, zipfile

import os, re, logging

# Module imports
from api_functions import (
    query_string_parser,
    proxify_response,
    easy_pagination,
    ResourceMap,
    APIResponse
)

from authz import JWT

from coredb.rds.bump.models import (
    Lot,
    DeviceType,
    ProductSKU,
    DeviceInventory,
    User,
    Association
)

# Layer imports
from coredb import CoreDB
from coredb.rds.bump import Bump

from sqlalchemy.dialects import mysql
from sqlalchemy import func, distinct, cast, Date
from sqlalchemy.orm import join

# Logger
logger = logging.getLogger()
FORMAT = "{'log_type':%(levelname)s, 'lambda_id':%(aws_request_id)s, 'message':%(message)s}"
for h in logger.handlers:
  h.setFormatter(logging.Formatter(FORMAT))
logger.setLevel(logging.DEBUG)


# Hack for reusing lambda connections
# NOTE: Remove when RDS Proxy in place
bump_rds_conn = None


def lambda_handler(event, context):

    handler_start = datetime.utcnow()

    # # Logging
    # NOTE: Something in lambda is interfering with custom lambda handlers and formats

    # LOG: Request
    logger.info({
        "self": {
            "aws_request_id": context.aws_request_id,
            "version": context.function_version,
            "log_group": context.log_group_name
        },
        "request": {
            "id": event["requestContext"]["requestId"],
            "http_method": event["httpMethod"],
            "stage": event["requestContext"]["stage"]
        },
        "identity": {
            "ip": event["requestContext"]["identity"]["sourceIp"],
            "user_agent": event["requestContext"]["identity"]["userAgent"]
        }
    })

    # Extract
    jwt = JWT(event["headers"]["Authorization"].split(" ")[1])

    # LOG: Identity
    logger.info({
        "identity": {
            "name": jwt.decoded_jwt["payload"]["user"]["name"],
            "email": jwt.decoded_jwt["payload"]["user"]["email"],
            "roles": jwt.decoded_jwt["payload"]["user"]["roles"],
            "audience": jwt.decoded_jwt["payload"]["user"]["aud"]
        },
        "session": {
            "session_key": jwt.decoded_jwt["payload"]["session_key"],
            "session_expiry": jwt.decoded_jwt["payload"]["user"]["exp"],
            "session_init": jwt.decoded_jwt["payload"]["user"]["iat"]
        }
    })

    # Initiate Sessions
    db = get_connection()

    # Get Session
    session = db.get_session()

    # get path elements
    path_list = list(filter(None, event["path"].split("/")))

    # Handle Root Call for testing
    if not path_list:
        return proxify_response(200, "QEngine RDS Root")

    if path_list[0] == "view":

        # Extract 'view' path parameter
        view = event["pathParameters"]["view"].lower()

        # Handle inventory view or task
        if view == "inventory":

            if len(path_list) > 2:
                if path_list[2] == "bake":
                    response = view_baker(event, session, "inventory")
                elif path_list[2] == "download":
                    response = download_view(event, session, "inventory")
                else:
                    response = proxify_response(400, f"Unknown Path element {path_list[2]}")
            else:
                response = inventory_view(event, session)

        # Handle users view or task
        elif view == "users":
            if len(path_list) > 2:
                if path_list[2] == "bake":
                    response = view_baker(event, session, "users")
                elif path_list[2] == "download":
                    response = download_view(event, session, "users")
                else:
                    response = proxify_response(400, f"Unknown Path element {path_list[2]}")
            else:
                response = users_view(event, session)
        else:
            response = proxify_response(404, f"View not found")
    else:
        proxify_response(404, "Nothing is here")

    # TODO: Extend session to accept with context manager? try/exc block?
    session.close()

    # pretty much just prints out everything in the log
    logger.info({
        "response": {
            "status_code": response["statusCode"],
            "headers": response["headers"]
        },
        "self": {
            "remaining_ttl": context.get_remaining_time_in_millis()
        }
    })

    return response

def view_baker(event, session, view="inventory"):

    resource_map = ResourceMap(view)

    filter_options = {}

    for reference, resource in resource_map.data.items():

        filter_options[reference] = []

        if reference == "date_added":
            for value in session.query(cast(resource["obj"], Date)).distinct():
                filter_options[reference].extend(value)
        else:
            for value in session.query(resource["obj"]).distinct():
                filter_options[reference].extend(value)

    return proxify_response(200, filter_options)

def inventory_view(event, session):

    view_start_timestamp = datetime.now()

    # Define view
    view = "inventory"

    # defaults & instants
    metadata = {}
    filters = []
    orders = []

    # UI Field to resource map for ordering and filtering
    resource_map = ResourceMap(view)

    # # Pagination # #
    page, page_size, pagination_errors = easy_pagination(event)

    # Add pagination errors if any exist
    if pagination_errors:
        metadata["errors"] = {"pagination_errors": pagination_errors}

    # Define filters and orders
    filters = resource_map.filters(event, target="DB")
    orders = resource_map.orderables(event, target="DB")

    # metadata: Add skipped to meta
    metadata["count"] = {"skipped": page*page_size if page_size else 0}

    # Build complete table query
    # NOTE: Leaving "Complete" until desired resources are locked
    inventory = session.query(
        DeviceInventory,
        DeviceType, ProductSKU,
        Lot,
        Association, User) \
        .outerjoin(
            DeviceInventory.lot,
            DeviceInventory.device_type,
            DeviceInventory.associations) \
        .outerjoin(
            DeviceType.sku,
            Association.user) \
        .with_entities(*resource_map.objs())

    # Add default order if none are specified
    if not orders:
        orders = [DeviceInventory.created_at.desc()]

    # Query: Order and filter
    inventory = inventory \
        .filter(*filters) \
        .order_by(*orders)

    # metadata: Snatch total count
    metadata["count"]["total"] = inventory.count()

    # metadata: add page of pagination to meta
    metadata["page"] = page


    metadata["time"] = {}
    metadata["time"]["query_building"] = datetime.now() - view_start_timestamp
    query_start_timestamp = datetime.now()

    logger.debug({
        "request": {
            "bound_query": str(inventory.statement.compile(compile_kwargs={"literal_binds": True}))
        }
    })

    # Unpack data
    data = []
    for record in inventory[page*page_size:(page+1)*page_size]:
        data.append(record._asdict())

    # metadata: snatch records returned
    metadata["count"]["returned"] = len(data)

    metadata["time"]["query_execution"] = datetime.now() - query_start_timestamp
    metadata["payload_size"] = getsizeof(data)

    ret_obj = {
        "metadata": metadata,
        "data": data
    }

    return proxify_response(200, ret_obj)

def users_view(event, session):

    view_start_timestamp = datetime.now()

    # Define View
    view = "users"

    # defaults & instants
    metadata = {}
    filters = []
    orders = []

    # UI Field to resource map for ordering and filtering
    resource_map = ResourceMap(view)

    # # Pagination # #
    page, page_size, pagination_errors = easy_pagination(event)

    # Add pagination errors if any exist
    if pagination_errors:
        metadata["errors"] = {"pagination_errors": pagination_errors}

    # # Filtering and Ordering # #
    filters = resource_map.filters(event, target="DB")
    orders = resource_map.orderables(event, target="DB")

    # meta
    metadata["count"] = {"skipped": page*page_size if page_size else 0}

    # Build complete table
    users = session.query(User) \
        .outerjoin(
            Association.user,
            Association.device) \
        .with_entities(*resource_map.objs())

    # Add default order if none are specified
    if not orders:
        orders = [User.date_added.desc()]

    # Query: Order and filter
    users = users \
        .filter(*filters) \
        .order_by(*orders)

    # metadata: add page of pagination to meta
    metadata["page"] = page
    metadata["time"] = {}
    metadata["time"]["query_building"] = datetime.now() - view_start_timestamp
    query_start_timestamp = datetime.now()

    # Unpack and group by subject_id (Focus of Users table)
    users_grouped = groupby(users, lambda x: x.subject_id)

    # Iterate over flattened records and serialize l1
    start_index, end_index = page*page_size, (page+1)*page_size

    for index, (subject_id, record_group) in enumerate(users_grouped):

        if index < page*page_size:
            continue
        elif index > (page+1)*page_size:
            break

        record_buffer = {}
        record_buffer["subject_id"] = subject_id
        # record_buffer["date_added"] = record_group[0].strftime("%m/%d/%y")

        # Extract grouped information
        record_buffer["stim"] = []
        record_buffer["basestation"] = []
        record_buffer["other_sn"] = []
        record_buffer["unassigned_device_count"] = 0
        record_buffer["current_device_count"] = 0
        record_buffer["unassigned"] = []

        # Iterate
        data = []
        for group in record_group:

            if group[resource_map.loc("unassigned_date")] == None:
                # NOTE:Indicates the device has never been unassigned and therefore
                # is currently "assigned" to the patient

                record_buffer["current_device_count"] += 1

                if group[resource_map.loc("serial_number")][:1] == "S":
                    record_buffer["stim"].append(
                        {
                            "serial_number": group[resource_map.loc("serial_number")],
                            "assigned_date": group[resource_map.loc("assigned_date")].strftime("%m/%d/%y")
                        }
                    )

                elif group[resource_map.loc("serial_number")][:1] == "B":
                    record_buffer["basestation"].append(
                        {
                            "serial_number": group[resource_map.loc("serial_number")],
                            "assigned_date": group[resource_map.loc("assigned_date")].strftime("%m/%d/%y")
                        }
                    )
                else:
                    record_buffer["other"].append(
                        {
                            "serial_number": group[resource_map.loc("serial_number")],
                            "assigned_date": group[resource_map.loc("assigned_date")].strftime("%m/%d/%y")
                        }
                    )
            else:
                record_buffer["unassigned_device_count"] += 1
                record_buffer["unassigned"].append({
                    "serial_number": group[resource_map.loc("serial_number")],
                    "assigned_date": group[resource_map.loc("assigned_date")].strftime("%m/%d/%y"),
                    "unassigned_date": group[resource_map.loc("unassigned_date")].strftime("%m/%d/%y")
                })

        # Add in agg for display
        data.append(record_buffer)

    # metadata: snatch records returned
    metadata["count"]["returned"] = len(data)

    metadata["time"]["query_execution"] = datetime.now() - query_start_timestamp
    metadata["payload_size"] = getsizeof(data)

    ret_obj = {
        "metadata": metadata,
        "data": data
    }

    return proxify_response(200, ret_obj)


def download_view(event, session, view="inventory"):

    if view == "users":

        view_start_timestamp = datetime.now()

        # Define View
        view = "users"

        # defaults & instants
        metadata = {}
        filters = []
        orders = []

        # UI Field to resource map for ordering and filtering
        resource_map = ResourceMap(view)

        # # Filtering and Ordering # #
        filters = resource_map.filters(event, target="DB")
        orders = resource_map.orderables(event, target="DB")

        # Build complete table
        users = session.query(User) \
            .outerjoin(
                Association.user,
                Association.device) \
            .with_entities(*resource_map.objs())

        # Add default order if none are specified
        if not orders:
            orders = [User.date_added.desc()]

        # Query: Order and filter
        users = users \
            .filter(*filters) \
            .order_by(*orders)


        nlist = [[col["name"] for col in users.column_descriptions]]

        # Unpack and write record values
        total_len = users.count()

        for index, record in enumerate(users):
            nlist.append([str(col_val) for col_val in record])

    elif view == "inventory":

        # defaults & instants
        metadata = {}
        filters = []
        orders = []

        # UI Field to resource map for ordering and filtering
        resource_map = ResourceMap(view)

        # Define filters and orders
        filters = resource_map.filters(event, target="DB")
        orders = resource_map.orderables(event, target="DB")

        # Build complete table query
        # NOTE: Leaving "Complete" until desired resources are locked
        inventory = session.query(
            DeviceInventory,
            DeviceType, ProductSKU,
            Lot,
            Association, User) \
            .outerjoin(
                DeviceInventory.lot,
                DeviceInventory.device_type,
                DeviceInventory.associations) \
            .outerjoin(
                DeviceType.sku,
                Association.user) \
            .with_entities(*resource_map.objs())

        # Add default order if none are specified
        if not orders:
            orders = [DeviceInventory.created_at.desc()]

        # Query: Order and filter
        inventory = inventory \
            .filter(*filters) \
            .order_by(*orders)

        nlist = [[col["name"] for col in inventory.column_descriptions]]

        for index, record in enumerate(inventory):
            nlist.append([str(col_val) for col_val in record])

    # Make csv
    csv_str = ""
    for row in nlist:
        csv_str += ",".join(row) + "\n"

    # Encode to Byte
    encoded_csv = csv_str.encode()

    # Byte Compress
    compressed_encoded_csv = zlib.compress(encoded_csv)

    ret_obj = {
        "metadata": metadata,
        "data": compressed_encoded_csv
    }

    return proxify_response(200, ret_obj)


# # # Helper Functions # # #
def get_connection():

    global bump_rds_conn

    # init RDS connection
    bump_rds_uri = os.environ.get('RDS_URI_PATH', None)
    bump_rds_usr = os.environ.get('RDS_USER_PATH', None)
    bump_rds_pwd = os.environ.get('RDS_PW_PATH', None)
    bump_rds_db = os.environ.get('RDS_DB_PATH', None)

    parameters = [
        bump_rds_uri,
        bump_rds_usr,
        bump_rds_pwd
    ]

    response = CoreDB.ssm.get_parameters(parameters, withDecryption=True)

    try:
        if bump_rds_conn:
            pass
        else:
            bump_rds_conn = Bump(response[bump_rds_uri], response[bump_rds_usr], response[bump_rds_pwd], bump_rds_db)
    except Exception as Err:
        raise ConnectionError(Err)

    return bump_rds_conn