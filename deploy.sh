#!/bin/sh

PROFILE="RS-dev"

BUCKET="qengine-api-v1-dev"

# Creates your deployment bucket if it doesn't exist yet.
aws2 s3 mb s3://$BUCKET --profile $PROFILE

# Uploads files to S3 bucket and creates CloudFormation template
sam package --template-file template.yaml \
    --s3-bucket $BUCKET \
    --output-template-file package.yaml \
    --profile $PROFILE

# Deploys your stack
sam deploy --template-file package.yaml \
    --stack-name $BUCKET \
    --capabilities CAPABILITY_IAM \
    --profile $PROFILE