
# Core imports
from datetime import datetime
import json
import os

# Layer imports
from coredb import CoreDB
from coredb.atlas import Atlas

def lambda_handler(event, context):

    path_list = event["path"].split("/")

    # Handle Root Call for testing
    if  len(path_list) == 0:
        return proxify_response(200, "QEngine Atlas Root")

    if path_list[0] == "devices":
        # Handle from a devices perspective
        pass

    elif path_list[0] == "users":
        # Handle from a user perspective
        if len(path_list) == 3:
            if path_list[2] == "therapysessions":
                # Return all therapy sessions for a single subject
                response = proxify_response(200, "Implement Agg Therapy Sessions call")
                response = proxify_response(200, users_get_one_stimlogs(subject_id))
        else:
            # Handle base call
            pass

    return response

def get_connection():

    # init RDS connection
    atlas_uri = os.environ.get('SSM_CLUSTER_URI_PATH', None)
    atlas_user = os.environ.get('SSM_CLUSTER_USER_PATH', None)
    atlas_password = os.environ.get('SSM_CLUSTER_PW_PATH', None)
    atlas_db = os.environ.get('SSM_CLUSTER_DB_PATH', None) # unused atm

    parameters = [
        atlas_uri,
        atlas_user,
        atlas_password
    ]

    response = CoreDB.ssm.get_parameters(parameters, withDecryption=True)

    atlas_db = Atlas(response[atlas_uri], response[atlas_user], response[atlas_password])

    return atlas_db


def proxify_response(code, body, **headers):
    """Lambda Proxy takes a different format than Lambda, this builds that.

    :param code: The HTTP response code to pass forward
    :type code: int
    :param body: The Body (content) of the response
    :type body: dict
    :return: Lamnda Proxy response
    :rtype: dict
    """

    return {
        "isBase64Encoded": False,
        "statusCode": code,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            **headers},
        "body": dumps(body, default=str)
    }